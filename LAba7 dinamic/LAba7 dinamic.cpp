#include "pch.h"
#include <iostream>

class matrix
{
private:

	int **Arr;
	int rows;
	int columns;



public:
	matrix(); //констурктор
	~matrix(); //деструкор

	int **p_Arr;
	int p_rows;
	int p_columns;


	void input();
	void print(); // ф-я вывода

	int getrows();
	int getcolumns();
	int getelem();
	int prisv();
	int ret();
	void del();
	int getElement(int siz, int hig);


	bool summMatrix(matrix matr2); //ф-я суммы
	bool multMatrix(matrix matr2); //ф-я произведения
	bool transp(); // ф-я транспонирования

	
};



matrix::matrix()
{
}

matrix::~matrix()
{
}



int matrix::getElement(int siz, int hig)
{
	if (siz<rows && hig<columns && siz>-1 && siz>-1)
	{
		return p_Arr[siz][hig];
	}
	std::cout << "Connot get elenent. Index Error!\n";
	return	-1;
}


void matrix::input()
{
	std::cout << "Введите кол-во строк: ";
	std::cin >> rows;
	std::cout << "Введите кол-во столбцов: ";
	std::cin >> columns;
	std::cout << "\n";

	Arr = new int*[rows];

	for (int i = 0; i < rows; i++)
	{
		Arr[i] = new int[columns];
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			Arr[i][j] = rand() % 10;
		}
	}
}

void matrix::print()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			std::cout << Arr[i][j] << "\t";
		}
		std::cout << "\n";
	}
}

int matrix::getrows()
{
	return rows;
}

int matrix::getcolumns()
{
	return columns;
}

int matrix::getelem()

{
	p_rows = rows;
	p_columns = columns;

	p_Arr = new int*[p_rows];
	for (int i = 0; i < p_rows; i++)
	{
		p_Arr[i] = new int[p_columns];
	}
	for (int i = 0; i < p_rows; i++)
		for (int j = 0; j < p_columns; j++)
		{
			p_Arr[i][j] = Arr[i][j];
		}
	return 0;
}


int matrix::prisv()
{
	p_rows = rows;
	p_columns = columns;

	for (int i = 0; i < p_rows; i++)
	{
		for (int j = 0; j < p_columns; j++)
		{
			p_Arr[i][j] = Arr[i][j];
		}
	}
	return 0;
}

int matrix::ret()
{
	rows = p_rows;
	columns = p_columns;
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			Arr[i][j] = p_Arr[i][j];
		}
	}
	return 0;
}

void matrix::del()
{
	for (int i = 0; i < rows; i++)
	{
		delete[]Arr[i];
	}
	delete[]Arr;

	for (int i = 0; i < p_rows; i++)
	{
		delete[]p_Arr[i];
	}
	delete[]p_Arr;

}

bool matrix::summMatrix(matrix matrB)
{
	if ((rows == matrB.rows) & (columns == matrB.columns))
	{
		int tt[10][10];
		for (int a = 0; a < rows; a++)
		{
			for (int b = 0; b < columns; b++)
			{
				tt[a][b] = Arr[a][b];
			}
		}
		std::cout << "\n\nСумма матриц равна\n\n";

		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				tt[i][j] += matrB.Arr[i][j];
			}
		}
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				std::cout << tt[i][j] << "\t";
			}
			std::cout << "\n";
		}

		return true;

	}
	std::cout << "\n\nОшибка в сумме\n";
	return false;
}


bool matrix::multMatrix(matrix  matrB)
{

	if (columns == matrB.rows)
	{
		double **c = new double*[rows];
		c = new double*[rows];
		for (int i = 0; i < rows; i++)
		{
			c[i] = new double[matrB.columns];
			for (int j = 0; j < matrB.columns; j++)
			{
				c[i][j] = 0;
				for (int k = 0; k < columns; k++)
					c[i][j] += Arr[i][k] * matrB.Arr[k][j];
			}
		}

		std::cout << "\n\nМатрица произведения\n" << std::endl;
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < matrB.columns; j++)
				std::cout << c[i][j] << "\t";
			std::cout << std::endl;
		}

		for (int i = 0; i < rows; i++)
		{
			//for (int j = 0; j < matrB.columns; j++)
			//{
			delete[] c[i];
			//}
			//delete[] c;
		}
		return true;
	}


	std::cout << "\nОшибка в произведении\n";
	return false;
}

bool matrix::transp()
{
	if (rows == columns) {
		std::cout << "\nTранспонированная матрица A: \n\n";

		int arr1[10][10];
		int t = rows;
		rows = columns;
		columns = t;
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				arr1[i][j] = Arr[j][i];
				Arr[j][i] = arr1[i][j];
			}
		}

		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				std::cout << arr1[i][j] << "\t";
			}
			std::cout << "\n";
		}

		return false;
	}
	else {
		std::cout << "Ошибка в транспонировании!";
	}
}

class Vector : public matrix
{
public:
	double scalMultVector(Vector vec2);
	double MultVector(int num);
};
//get_size() get_size() A.get_high
double Vector::MultVector(int num) {
	for (int i = 0; i < this->getrows(); i++) {
		this->ret(0, i, this->getElement(0, i) * num);
	}
	return 1;
}

double Vector::scalMultVector(Vector vec2) {
	if (this->getrows() != vec2.getrows()) {
		return 0;
	}
	for (int i = 0; i < vec2.getrows(); i++) {
		this->setElem(0, i, this->getElement(0, i) * vec2.getElement(0, i));
	}
	return 1;
}

int main()
{
	setlocale(LC_ALL, "Russian");

	matrix matrA, matrB;
	std::cout << "A \n";
	matrA.input();
	matrA.getelem();
	matrA.print();

	std::cout << "\n";
	std::cout << "B\n";
	matrB.input();
	//matrB.prisv();
	matrB.print();

	std::cout << "\n\n------------------------------------------------------------------\n\n";
	matrA.summMatrix(matrB);
	//matrA.ret();

	matrA.multMatrix(matrB);
	//matrA.ret();

	matrA.transp();

	matrA.del();
	matrB.del();
	getchar();
	return 0;
}