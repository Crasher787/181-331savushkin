#include <iostream>
#include <string>
#include <queue>

int main()
{
	std::queue<std::string> myQueue_9;
	std::queue<std::string> myQueue_10;
	std::queue<std::string> myQueue_11;

	int input_class;
	std::cin >> input_class;

	while (input_class != 0)
	{

		if (input_class == 9)
		{
			std::string student;
			std::cin >> student;
			myQueue_9.push(student);
		}

		if (input_class == 10)
		{
			std::string student;
			std::cin >> student;
			myQueue_10.push(student);
		}

		if (input_class == 11)
		{
			std::string student;
			std::cin >> student;
			myQueue_11.push(student);
		}

		std::cin >> input_class;

	}

	while (!myQueue_9.empty())
	{
		std::cout << myQueue_9.front() << std::endl;
		myQueue_9.pop();
	}

	while (!myQueue_10.empty())
	{
		std::cout << myQueue_10.front() << std::endl;
		myQueue_10.pop();
	}

	while (!myQueue_11.empty())
	{
		std::cout << myQueue_11.front() << std::endl;
		myQueue_11.pop();
	}
}